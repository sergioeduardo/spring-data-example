package com.springdata.example.services;

import com.springdata.example.entities.Student;
import com.springdata.example.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {
    @Autowired
    public StudentRepository studentRepository;

    public void createStudent(Student student) {
        studentRepository.save(student);
    }

    public List<Student> getStudents() {
        return (List<Student>) studentRepository.findAll();
    }

    public String getStudentNameOnly(Integer id) {
        return studentRepository.getNameOnly(id);
    }

    public void deleteStudent(Integer id) {
        Student student = studentRepository.findById(id).get();
        studentRepository.delete(student);
    }

    public void updateStudentName(Integer id, String studentName) {
        Student student = studentRepository.findById(id).get();
        student.name = studentName;
        studentRepository.save(student);
    }
}
