package com.springdata.example.controllers;

import com.springdata.example.entities.Student;
import com.springdata.example.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/students")
@CrossOrigin(origins = "*")
public class StudentsController {
    @Autowired
    public StudentService studentService;

    @PostMapping("/create")
    public void create(@Valid @RequestBody Student student) {
        studentService.createStudent(student);
    }

    @GetMapping("/getStudents")
    public List<Student> getStudents() {
        return studentService.getStudents();
    }

    @GetMapping("/getStudentNameOnly/{id}")
    public String getStudentNameOnly(@PathVariable(value = "id") Integer id) {
        return studentService.getStudentNameOnly(id);
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable(value = "id") Integer id) {
        studentService.deleteStudent(id);
    }

    @PutMapping("/update/{id}")
    public void update(@PathVariable(value = "id") Integer id, @RequestParam(name = "name") String name) {
        studentService.updateStudentName(id, name);
    }
}
