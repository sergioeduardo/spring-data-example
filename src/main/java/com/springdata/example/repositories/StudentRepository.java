package com.springdata.example.repositories;

import com.springdata.example.entities.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends CrudRepository<Student, Integer> {

    //Obtener solamente el tiempo de la categoría asignada al ticket
    @Query("SELECT name FROM Student student WHERE id = :id")
    String getNameOnly(@Param("id") Integer id);
}
